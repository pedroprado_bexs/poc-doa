package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/joho/godotenv"
	application "temis.doa/Application"
	apitranslator "temis.doa/Application/Translator"
	fileservice "temis.doa/Domain/External/file"
	simplyservice "temis.doa/Domain/External/simply"
	simplyclient "temis.doa/Domain/External/simply/http"
	translator "temis.doa/Domain/Service/Translator"
	pubsubInfra "temis.doa/Infra/pubsub"
	infra "temis.doa/Infra/storage/S3"

	repository "temis.doa/Domain/Repository"
	domainservice "temis.doa/Domain/Service"
	"temis.doa/Infra/simply"
	router "temis.doa/Router"
)

var port = os.Getenv("SERVER_PORT")

var webClient = &http.Client{
	Timeout: time.Duration(20 * time.Second),
}

var ctx context.Context
var client *pubsub.Client

func main() {
	_ = godotenv.Load(".env")
	_, err := simply.Authenticate()
	if err != nil {
		log.Println("[Main] Error logging in Simply: ", err)
	}

	ctx = context.Background()
	client = pubsubInfra.OpenPubSubConnection(ctx, "doa-project")

	//-----------------Validator
	validationRepository := repository.NewValidationRepository()
	scoreService := domainservice.NewScoreService()
	callbackService := domainservice.NewCallbackService()

	validatorService := domainservice.NewValidatorService(validationRepository, scoreService, callbackService)
	go validatorService.Validate()

	//-----------------Analysis
	s3Service := infra.NewS3Service()
	fileService := fileservice.NewFileService(s3Service)

	simplyClient := simplyclient.NewSimplyClient(webClient)
	simplyService := simplyservice.NewSimplyService(simplyClient)

	analysisQueueService := domainservice.NewPubsubService(&ctx, client, "requestcode", "requestcode")

	simplyDomainTranslator := translator.NewSimplyDomainTranslator()

	analysisService := domainservice.NewAnalysisService(fileService, simplyService, analysisQueueService, simplyDomainTranslator)
	go analysisService.RetrieveExtraction()

	//-----------------Validation
	domainTranslator := apitranslator.NewDomainTranslator()

	validationService := application.NewValidationService(domainTranslator, analysisService)

	validationController := application.NewValidationController(validationService)

	router.StartWebApp(":"+port, validationController)
}
