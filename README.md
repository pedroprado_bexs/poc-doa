# POC para utilização da API da Simply para OCR

## Instalando projeto
Clone o projeto.

Use: go mod download para baixar as dependências.

## Iniciando 
Coloque uma imagem de documento (RG, CNH) na raiz do projeto.

Altere a variável fileName (do arquivo analysis-service.go) para o nome do arquivo colocado na raiz.

Inicie o projeto com: go run main.go

## Utilizando 

Buscar resultado: Use o código da solicitação no endpoint "/extraction/begin" . O arquivo do Root será carregado e o programa começará a fazer um polling na Api da Simply.
Quando o resultados estiver pronto, este será impresso no console.

## Arquitetura Detalhada

![Scheme](DOA-Arquitetura_Detalhada_DDD.png)
