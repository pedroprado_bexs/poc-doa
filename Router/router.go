package router

import (
	"log"

	"github.com/gin-gonic/gin"
	application "temis.doa/Application"
)

var (
	router = gin.Default()
)

//StartWebApp initializes a web server with Analysis Routes
func StartWebApp(port string, validationController application.IValidationController) {

	router.POST("/extraction/begin", validationController.BeginAnalysis)

	if err := router.Run(port); err != nil {
		log.Println("[Router] Error: ", err)
		panic(err)
	}
}
