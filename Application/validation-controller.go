package application

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/gin-gonic/gin"
	dto "temis.doa/Application/Dto"
)

type IValidationController interface {
	BeginAnalysis(ctx *gin.Context)
}

type validationController struct {
	validationService IValidationService
}

func init() {}

func NewValidationController(validationService IValidationService) IValidationController {
	return &validationController{
		validationService: validationService,
	}
}

//BeginAnalysis creates a request for starting an analysis in Simply Atomics
func (ref *validationController) BeginAnalysis(ctx *gin.Context) {
	request := &dto.DocumentAnalysisRequest{}
	bs, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		log.Println("[AnalysisController] Error reading body: ", err.Error())
	}
	err2 := json.Unmarshal(bs, &request)
	if err2 != nil {
		ctx.JSON(500, gin.H{"error": err2.Error()})

	} else {
		requestCode, err := ref.validationService.BeginAnalysis(request)
		if err != nil {
			ctx.JSON(500, gin.H{"error": err.Error()})

		} else {
			ctx.JSON(200, gin.H{"message": "Anaálise Criada com Sucesso", "codigoRequisicao": requestCode})
		}
	}
}
