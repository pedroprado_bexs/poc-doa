package application

import (
	dto "temis.doa/Application/Dto"
	translator "temis.doa/Application/Translator"
	analysisservice "temis.doa/Domain/Service"
	validationservice "temis.doa/Domain/Service"
)

type IValidationService interface {
	BeginAnalysis(request *dto.DocumentAnalysisRequest) (int, error)
}

type validationService struct {
	domainTranslator translator.IDomainTranslator
	analysisService  analysisservice.IAnalysisService
	validatorService validationservice.IValidatorService
}

func init() {}

func NewValidationService(domainTranslator translator.IDomainTranslator, analysisService analysisservice.IAnalysisService) IValidationService {
	return &validationService{
		domainTranslator: domainTranslator,
		analysisService:  analysisService,
	}
}

//BeginAnalysis starts an analysis
func (ref *validationService) BeginAnalysis(request *dto.DocumentAnalysisRequest) (int, error) {

	document := ref.domainTranslator.ToDomain(request)

	requestCode, err := ref.analysisService.BeginExtraction(document)
	if err != nil {
		return 0, err
	}
	return requestCode, nil
}
