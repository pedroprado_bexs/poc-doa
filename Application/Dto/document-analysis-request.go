package dto

import "github.com/google/uuid"

type DocumentAnalysisRequest struct {
	ProfileID      uuid.UUID       `json:"ProfileID"`
	Type           DocumentType    `json:"DocumentType"`
	Side           SideType        `json:"DocumentSide"`
	Metadata       *MetadataDto    `json:"Metadata"`
	FileParams     *FileParams     `json:"FileParams"`
	OCRParams      *OCRParams      `json:"OCRParams"`
	CallbackParams *CallbackParams `json:"CallbackParams"`
}

type MetadataDto struct {
	NomeTexto           string `json:"Name"`
	NomeMaeTexto        string `json:"MothersName"`
	DataNascimentoTexto string `json:"BirthDay"`
	RegistroTexto       string `json:"DocumentNumber"`
	IdentidadeTexto     string `json:"Identification"`
}

type FileParams struct {
	FileID   uuid.UUID `json:"FileID"`
	FileURL  string    `json:"FileURL"`
	FileName string    `json:"FileName"`
}

type OCRParams struct {
	Provider OCRProvider `json:"Provider"`
}

type CallbackParams struct {
	URL string `json:"URL"`
}

type CalculatorParams struct {
}

//SimplyParams represents the parameters used in the analysys
type SimplyParams struct {
	LegacyCode string `json:"CodigoLegado,omitempty"`
	Profile    string `json:"PerfilTipificacao,omitempty"`
	Extract    bool   `json:"ExtrairDados,omitempty"`
}

type DocumentType string

const (
	RG  DocumentType = "RG"
	CNH DocumentType = "CNH"
)

type SideType string

const (
	FRENTE SideType = "FRENTE"
	VERSO  SideType = "VERSO"
	TUDO   SideType = "AMBOS"
)

type OCRProvider string

const (
	SIMPLY OCRProvider = "SIMPLY"
)
