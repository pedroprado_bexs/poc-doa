package adpter

import (
	dto "temis.doa/Application/Dto"
	entity "temis.doa/Domain/Entity"
)

type IDomainTranslator interface {
	ToDomain(documentRequest *dto.DocumentAnalysisRequest) *entity.Document
}

type domainTranslator struct {
}

func NewDomainTranslator() IDomainTranslator {
	return &domainTranslator{}
}

func (ref *domainTranslator) ToDomain(documentRequest *dto.DocumentAnalysisRequest) *entity.Document {
	metadata := entity.Metadata{
		NomeTexto:           documentRequest.Metadata.NomeTexto,
		NomeMaeTexto:        documentRequest.Metadata.NomeMaeTexto,
		DataNascimentoTexto: documentRequest.Metadata.DataNascimentoTexto,
		RegistroTexto:       documentRequest.Metadata.RegistroTexto,
		IdentidadeTexto:     documentRequest.Metadata.IdentidadeTexto,
	}
	fileData := entity.File{
		FileID:   documentRequest.FileParams.FileID,
		FileURL:  documentRequest.FileParams.FileURL,
		FileName: documentRequest.FileParams.FileName,
	}
	entity := entity.Document{
		ProfileID: documentRequest.ProfileID,
		Type:      getDocumentType(documentRequest.Type),
		Side:      getDocumentSide(documentRequest.Side),
		Metadata:  &metadata,
		File:      &fileData,
	}

	return &entity
}

func getDocumentType(documentType dto.DocumentType) entity.DocumentType {
	switch documentType {
	case "RG":
		return entity.RG
	case "CNH":
		return entity.CNH
	default:
		return "RG"
	}
}

func getDocumentSide(documentSide dto.SideType) entity.SideType {
	switch documentSide {
	case "FRENTE":
		return entity.FRENTE
	case "VERSO":
		return entity.VERSO
	case "TUDO":
		return entity.TUDO
	default:
		return "FRENTE"
	}
}
