module temis.doa

go 1.14

require (
	cloud.google.com/go/pubsub v1.6.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.3.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/martian v2.1.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/googleapis/gax-go v1.0.3 // indirect
	github.com/jinzhu/gorm v1.9.15
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/sys v0.0.0-20200805065543-0cf7623e9dbd // indirect
	golang.org/x/tools v0.0.0-20200804234916-fec4f28ebb08 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/api v0.30.0
	google.golang.org/appengine v1.6.6
	google.golang.org/genproto v0.0.0-20200804151602-45615f50871c // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gorm.io/gorm v0.2.25
)
