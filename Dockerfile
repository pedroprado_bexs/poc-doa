# Build phase

FROM golang:alpine AS builder

LABEL maintainer="Pedro Prado"

WORKDIR /app

COPY . .

RUN go build -o temis-doa .


# Copy phase
FROM alpine

WORKDIR /app

COPY --from=builder /app/temis-doa .

COPY --from=builder /app/RgTudo.jpg .

CMD ["./temis-doa"]