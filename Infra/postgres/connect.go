package postgres

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //imported for using postgres
	"temis.doa/Modules/Api/model"
)

var (
	pgHost     = os.Getenv("PGHOST")
	pgPort     = os.Getenv("PGPORT")
	pgUser     = os.Getenv("PGUSER")
	pgDatabase = os.Getenv("PGDATABASE")
	pgPassword = os.Getenv("PGPASSWORD")
	database   *gorm.DB
)

func init() {
	database = ConnectPostgres()
	InitUsers()
}

//ConnectPostgres opens a connection to a Postgres instance
func ConnectPostgres() *gorm.DB {
	connString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", pgHost, pgPort, pgUser, pgDatabase, pgPassword)
	connString = connString + " sslmode=disable"
	db, err := gorm.Open("postgres", connString)
	if err != nil {
		log.Panicln("[Infra] Error connecting postgres: ", err.Error())
		return nil
	}
	return db
}

//GetDB gets the Database instance
func GetDB() *gorm.DB {
	return database
}

func InitUsers() {
	database.CreateTable(&model.DOA{})
}
