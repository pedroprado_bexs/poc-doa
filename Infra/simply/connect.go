package simply

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	_ "github.com/joho/godotenv/autoload"
)

var (
	host          = os.Getenv("SIMPLY_HOST")
	loginURI      = os.Getenv("SIMPLY_LOGIN_URI")
	analysisURI   = os.Getenv("SIMPLY_ANALYSIS_URI")
	username      = os.Getenv("SIMPLY_USERNAME")
	password      = os.Getenv("SIMPLY_PASSWORD")
	webClient     *http.Client
	authorization string
)

func init() {
	webClient = &http.Client{
		Timeout: time.Duration(20 * time.Second),
	}
}

func Authenticate() (*SimplyLoginResponse, *SimplyLoginError) {
	URL := host + loginURI

	body := url.Values{}
	body.Add("username", username)
	body.Add("password", password)
	body.Add("grant_type", "password")

	request, _ := http.NewRequest("POST", URL, strings.NewReader(body.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	request.Header.Set("Accept", "application/json")

	response, err := webClient.Do(request)
	if err != nil {
		log.Println("[SimplyConnect] Error authentication: ", err)
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		loginError := SimplyLoginError{}
		err := json.NewDecoder(response.Body).Decode(&loginError)
		if err != nil {
			log.Println("[SimplyConnect] Error decoding response body: ", err)
		}
		return nil, &loginError
	}

	loginResponse := SimplyLoginResponse{}
	err2 := json.NewDecoder(response.Body).Decode(&loginResponse)
	if err2 != nil {
		log.Println("[SimplyConnect] Error decoding response body: ", err2)
	}
	authorization = "Bearer " + loginResponse.AccessToken
	fmt.Println(authorization)
	return &loginResponse, nil
}

func GetAuthorization() string {
	return authorization
}
