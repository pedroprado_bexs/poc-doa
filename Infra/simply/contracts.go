package simply

type SimplyLoginResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	Username    string `json:"userName"`
	Issued      string `json:".issued"`
	Expires     string `json:".expires"`
}

type SimplyLoginError struct {
	ErrorType        string `json:"error"`
	ErrorDescription string `json:"error_description"`
}
