package infra

import (
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"os"
)

type IS3Service interface {
	DownloadFile(fileId string)
}

type s3Service struct {
}

func init() {
	image.RegisterFormat("jpeg", "jpeg", jpeg.Decode, jpeg.DecodeConfig)
}

func NewS3Service() IS3Service {
	return &s3Service{}
}

func (ref *s3Service) DownloadFile(fileId string) {

	imgFile, err := os.Open("RgFrente.jpg")
	if err != nil {
		log.Println("[S3Provider] Error: ", err)
	}
	defer imgFile.Close()

	img, _, err := image.Decode(imgFile)
	if err != nil {
		log.Println("[S3Provider] Error: ", err)
	}

	fmt.Println(img)
}
