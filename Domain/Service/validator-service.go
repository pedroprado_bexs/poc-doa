package domainservice

import (
	"log"

	entity "temis.doa/Domain/Entity"
	repository "temis.doa/Domain/Repository"
)

type IValidatorService interface {
	Validate()
}

type validatorService struct {
	validationRepository repository.IValidationRepository
	scoreService         IScoreService
	callbackService      ICallbackService
}

func NewValidatorService(validationRepository repository.IValidationRepository, scoreService IScoreService, callbackService ICallbackService) IValidatorService {
	return &validatorService{
		validationRepository: validationRepository,
		scoreService:         scoreService,
		callbackService:      callbackService,
	}
}

func (ref *validatorService) Validate() {
	for {
		select {
		case document := <-*extractorValidatorChannel:
			validation := &entity.Validation{
				Document: document,
			}
			err := ref.validate(validation)
			if err != nil {
				log.Println("[ValidationService] Error validating: ", err.Error())
			}
		}
	}
}

//Validate calculates the score of similiarity between the document information sent and the OCR results
func (ref *validatorService) validate(validation *entity.Validation) error {

	score := ref.scoreService.CalculateScore(validation.Document)
	validation.Score = score

	err := ref.validationRepository.Save(validation)
	if err != nil {
		log.Println("[ValidationService] Error saving Validation to database: ", err.Error())
		return err
	}

	ref.callbackService.Callback(validation)

	return nil
}
