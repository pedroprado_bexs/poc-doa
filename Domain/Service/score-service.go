package domainservice

import entity "temis.doa/Domain/Entity"

type IScoreService interface {
	CalculateScore(document *entity.Document) *entity.Score
}

type scoreService struct{}

func NewScoreService() IScoreService {
	return &scoreService{}
}

//CalculateScore calculates the of document related to its OCR's analysis
func (ref *scoreService) CalculateScore(document *entity.Document) *entity.Score {

	return &entity.Score{
		Score: 9.5,
	}
}
