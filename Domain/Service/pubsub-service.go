package domainservice

import (
	"context"
	"log"
	"time"

	"cloud.google.com/go/pubsub"
)

type IPubsubService interface {
	Publish(message []byte) error
	Receive(process func(message []byte) error)
}

type pubsubService struct {
	ctx            *context.Context
	client         *pubsub.Client
	topicID        string
	subscriptionID string
}

func NewPubsubService(ctx *context.Context, client *pubsub.Client, topicID string, subscriptionID string) IPubsubService {
	return &pubsubService{
		ctx:            ctx,
		client:         client,
		topicID:        topicID,
		subscriptionID: subscriptionID,
	}
}

func (ref *pubsubService) Publish(message []byte) error {
	topic := ref.client.Topic(ref.topicID)
	result := topic.Publish(*ref.ctx, &pubsub.Message{
		Data: message,
	})

	id, err := result.Get(*ref.ctx)
	if err != nil {
		log.Println("[PubsubService] Error publishing message with id: ", id)
		return err
	}
	log.Println("[PubsubService] Published message with id: ", id)
	return nil
}

func (ref *pubsubService) Receive(process func(message []byte) error) {
	channel := make(chan *pubsub.Message)

	go pollingMessage(*ref.ctx, ref.client, ref.subscriptionID, channel)

	for {
		select {
		case message := <-channel:
			go func() {
				err := process(message.Data)
				if err != nil {
					log.Println("[PubsubService] Error processing message: ", message.ID)
				} else {
					message.Ack()
				}
			}()
		}
	}
}

func pollingMessage(ctx context.Context, client *pubsub.Client, subscriptionID string, channel chan *pubsub.Message) {
	subscription := client.Subscription(subscriptionID)
	subscription.ReceiveSettings.Synchronous = true
	subscription.ReceiveSettings.MaxOutstandingMessages = -1
	subscription.ReceiveSettings.MaxExtension = 120 * time.Second
	subscription.ReceiveSettings.MaxExtensionPeriod = -1

	for {
		err := subscription.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
			channel <- msg
			time.Sleep(500 * time.Millisecond)
		})
		if err != nil {
			log.Println("[PubsubService] Error pulling message from subscription: ", err)
			time.Sleep(20 * time.Second)
		}
	}
}
