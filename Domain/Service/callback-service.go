package domainservice

import (
	"log"

	entity "temis.doa/Domain/Entity"
)

type ICallbackService interface {
	Callback(validation *entity.Validation)
}

type callbackService struct{}

func NewCallbackService() ICallbackService {
	return &callbackService{}
}

func (ref *callbackService) Callback(validation *entity.Validation) {

	log.Println("[CallbackService] Validation sent to callback")
	log.Println("[CallbackService] Document Metadata")
	log.Printf("%+v \n", validation.Document.Metadata)

	log.Println("[CallbackService] OCR Metadata")
	log.Printf("%+v \n", validation.Document.OCRAnalysis.OcrMetadata)

}
