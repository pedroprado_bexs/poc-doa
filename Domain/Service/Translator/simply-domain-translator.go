package translator

import (
	entity "temis.doa/Domain/Entity"
	simply "temis.doa/Domain/External/simply/contracts"
)

type ISimplyDomainTranslator interface {
	Translate(simplyResult *simply.SimplyResult) *entity.OCRAnalysis
}

type simplyDomainTranslator struct{}

func init() {}

func NewSimplyDomainTranslator() ISimplyDomainTranslator {
	return &simplyDomainTranslator{}
}

func (ref *simplyDomainTranslator) Translate(simplyResult *simply.SimplyResult) *entity.OCRAnalysis {

	simplyMetadata := simplyResult.OCRFile[0].OCRFileType[0].Metadata

	documentType, documentSide := getFileTypeAndSide(simplyResult.OCRFile[0].OCRFileType[0].FileType)

	ocrMetadata := &entity.OcrMetadata{
		Assertividade:       simplyMetadata.Assertividade,
		NomeTexto:           simplyMetadata.NomeTexto,
		NomeMaeTexto:        simplyMetadata.NomeMaeTexto,
		DataNascimentoTexto: simplyMetadata.DataNascimentoTexto,
		RegistroTexto:       simplyMetadata.RegistroTexto,
		ExpedicaoTexto:      simplyMetadata.ExpedicaoTexto,
		IdentidadeTexto:     simplyMetadata.IdentidadeTexto,
	}

	ocrAnalysis := &entity.OCRAnalysis{
		RequestCode:  simplyResult.RequestCode,
		DocumentType: documentType,
		DocumentSide: documentSide,
		RequestDate:  simplyResult.RequestDate,
		ProcessDate:  simplyResult.ProcessDate,
		OcrMetadata:  ocrMetadata,
	}

	return ocrAnalysis
}

func getFileTypeAndSide(fileType int) (entity.DocumentType, entity.SideType) {
	switch fileType {
	case 103:
		return entity.RG, entity.FRENTE
	case 104:
		return entity.RG, entity.VERSO
	case 105:
		return entity.CNH, entity.FRENTE
	case 121:
		return entity.CNH, entity.VERSO
	default:
		return "", ""
	}
}
