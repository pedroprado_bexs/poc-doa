package domainservice

import (
	"encoding/json"
	"log"
	"time"

	entity "temis.doa/Domain/Entity"
	fileservice "temis.doa/Domain/External/file"
	simplyservice "temis.doa/Domain/External/simply"
	domaininfra "temis.doa/Domain/Infra/channel"
	translator "temis.doa/Domain/Service/Translator"
)

var (
	extractorValidatorChannel *chan *entity.Document
)

type IAnalysisService interface {
	BeginExtraction(document *entity.Document) (int, error)
	RetrieveExtraction()
}

type analysisService struct {
	fileService            fileservice.IFileService
	simplyService          simplyservice.ISimplyService
	queueService           IPubsubService
	simplyDomainTranslator translator.ISimplyDomainTranslator
}

func init() {
	extractorValidatorChannel = domaininfra.GetExtractorValidatorChannel()
}

func NewAnalysisService(fileService fileservice.IFileService, simplyService simplyservice.ISimplyService, queueService IPubsubService, simplyDomainTranslator translator.ISimplyDomainTranslator) IAnalysisService {
	return &analysisService{
		fileService:            fileService,
		simplyService:          simplyService,
		queueService:           queueService,
		simplyDomainTranslator: simplyDomainTranslator,
	}
}

//BeginExtraction attempts
func (ref *analysisService) BeginExtraction(document *entity.Document) (int, error) {

	file := ref.fileService.DownloadFile(document.File.FileID, document.File.FileURL)
	defer file.Close()

	requestCode, err := ref.simplyService.BeginExtraction(file, document.File.FileName)
	if err != nil {
		return 0, err
	}

	payload := &ChannelInfo{
		Document:    document,
		RequestCode: requestCode,
	}

	content, err := json.Marshal(payload)
	if err != nil {
		log.Println("[AnalysisService] Error parsing payload")
	}

	ref.queueService.Publish(content)

	return requestCode, nil
}

//RetrieveExtraction starts a fecthing result cycle, waiting for a message to come out of the channel
func (ref *analysisService) RetrieveExtraction() {

	ref.queueService.Receive(ref.retrieveResponse)

}

func (ref *analysisService) retrieveResponse(message []byte) error {
	channelInfo := &ChannelInfo{}
	if err := json.Unmarshal(message, channelInfo); err != nil {
		log.Println("[AnalysisService] Error parsing message to channelInfo")
	}

	log.Println("[AnalysisService] Received code: ", channelInfo.RequestCode)
	retrieved := false
	for !retrieved {
		response, err := ref.simplyService.RetrieveExtraction(channelInfo.RequestCode)
		if err != nil {
			log.Println("[AnalysisService] Error retrieving OCR result: ", err)
		} else {
			// fmt.Println("OCR: ", response.OCRFile[0].OCR)
			if response.OCRFile[0].OCR != "" {

				ocrAnalysis := ref.simplyDomainTranslator.Translate(response)
				document := channelInfo.Document
				document.OCRAnalysis = ocrAnalysis

				*extractorValidatorChannel <- document

				// fmt.Printf("%+v \n", response.OCRFile[0].OCRFileType[0].Metadata)
				retrieved = true
			} else {
				log.Println("[AnalysisService] Dormiu com código: ", channelInfo.RequestCode)
				time.Sleep(10 * time.Second)
			}
		}
	}
	return nil
}

type ChannelInfo struct {
	Document    *entity.Document
	RequestCode int
}
