package filecontracts

type ComplianceRequest struct {
	CustomerDocumentId string `json:"customerDocumentId"`
	FileId             string `json:"fileId"`
}

type ComplianceResponse struct {
	CustomerDocumentId string         `json:"customerDocumentId"`
	FileId             string         `json:"fileId"`
	ResponseFiles      []ResponseFile `json:"responseFiles"`
}

type ResponseFile struct {
	DocumentType             string       `json:"documentType"`
	IntegratorDocumentType   string       `json:"integratorDocumentType"`
	IntegratorDocumentTypeId int          `json:"integratorDocumentTypeId"`
	Metadata                 FileMetadata `json:"metadata"`
}

type FileMetadata struct {
	Assertividade  string
	Registro       string
	Expedicao      string
	Nome           string
	NomeMae        string
	DataNascimento string
}
