package externalservice

import (
	"image"
	"image/jpeg"
	"log"
	"os"

	infra "temis.doa/Infra/storage/S3"

	"github.com/google/uuid"
)

type IFileService interface {
	DownloadFile(fileID uuid.UUID, fileURL string) *os.File
}

type fileService struct {
	s3Service infra.IS3Service
}

func init() {
	image.RegisterFormat("jpeg", "jpeg", jpeg.Decode, jpeg.DecodeConfig)
}

func NewFileService(s3Service infra.IS3Service) IFileService {
	return &fileService{
		s3Service: s3Service,
	}
}

func (fs *fileService) DownloadFile(fileID uuid.UUID, fileURL string) *os.File {

	log.Println("[FileService] Id Arquivo: ", fileID)
	log.Println("[FileService] Url Arquivo: ", fileURL)
	imgFile, err := os.Open("RgTudo.jpg")
	if err != nil {
		log.Println("[FileService] Error openning document image: ", err)
	}
	return imgFile
}
