package simply

type SimplyError struct {
	Message    string     `json:"Message"`
	ModelState ModelState `json:"ModelState"`
}

type ModelState struct {
	Details []string `json:"detalhar"`
}
