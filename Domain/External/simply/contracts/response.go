package simply

type SimplyResult struct {
	OCRFile     []OCRFile `json:"Arquivos"`
	RequestCode int       `json:"CodigoSolicitacao"`
	LegacyCode  string    `json:"CodigoLegado"`
	RequestDate string    `json:"DataSolicitacao"`
	ProcessDate string    `json:"DataProcessamento"`
	Status      int       `json:"Status"`
	FileType    int       `json:"TipoArquivo"`
}

type OCRFile struct {
	OCR         string        `json:"OCR"`
	Name        string        `json:"Nome"`
	Hash        string        `json:"Hash"`
	OCRFileType []OCRFileType `json:"TipoArquivo"`
}

type OCRFileType struct {
	FileType          int         `json:"TipoArquivo"`
	FileTypeName      string      `json:"NomeTipoArquivo"`
	FullPage          bool        `json:"FolhaInteira"`
	IsSigned          bool        `json:"PossuiAssinatura"`
	DocumentVersion   interface{} `json:"VersaoDocument"`
	FilledFields      interface{} `json:"CamposPreenchidos"`
	HandwrittenFields interface{} `json:"CamposManuscritos"`
	ColoredImage      interface{} `json:"ImagemColorida"`
	PossibleFraud     interface{} `json:"PossivelFraude"`
	DuplicatedFields  interface{} `json:"PossuiComprovantesDuplicados"`
	Metadata          OcrMetadata `json:"Metadados"`
}

type OcrMetadata struct {
	Assertividade       string
	NomeTexto           string
	NomeMaeTexto        string
	DataNascimentoTexto string
	RegistroTexto       string
	ExpedicaoTexto      string
	IdentidadeTexto     string
}
