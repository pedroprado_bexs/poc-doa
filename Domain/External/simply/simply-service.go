package externalservice

import (
	"bytes"
	"errors"
	"io"
	"log"
	"mime/multipart"
	"os"
	"strconv"

	contracts "temis.doa/Domain/External/simply/contracts"
	httpclient "temis.doa/Domain/External/simply/http"
)

var (
	legacyCode    = "111"
	profileSimply = os.Getenv("SIMPLY_PROFILE")
	extractData   = "true"
)

type ISimplyService interface {
	BeginExtraction(file *os.File, fileName string) (int, error)
	RetrieveExtraction(requestCode int) (*contracts.SimplyResult, error)
}

type simplyService struct {
	simplyClient httpclient.ISimplyClient
}

func init() {
}

func NewSimplyService(simplyClient httpclient.ISimplyClient) ISimplyService {
	return &simplyService{
		simplyClient: simplyClient,
	}
}

//BeginExtraction attempts to begin an OCR analysis by Simply Atomics
func (ref *simplyService) BeginExtraction(file *os.File, fileName string) (int, error) {

	buffer := &bytes.Buffer{}
	bufferWriter := multipart.NewWriter(buffer)
	part, err := bufferWriter.CreateFormFile("Arquivo", fileName)
	if err != nil {
		log.Println("[SimplyService] Error creating field for document: ", err)
		return 0, errors.New(err.Error())
	}

	_, err = io.Copy(part, file)
	if err != nil {
		log.Println("[SimplyService] Error copying file to buffer: ", err)
		return 0, errors.New(err.Error())
	}

	_ = bufferWriter.WriteField("CodigoLegado", legacyCode)
	_ = bufferWriter.WriteField("PerfilTipificacao", profileSimply)
	_ = bufferWriter.WriteField("ExtrairDados", extractData)
	closeErr := bufferWriter.Close()
	if closeErr != nil {
		log.Println("[SimplyService] Error closing buffer: ", closeErr)
		return 0, errors.New(closeErr.Error())
	}

	requestCode, analysisError := ref.simplyClient.BeginExtraction(buffer, bufferWriter)
	if analysisError != nil {
		log.Println("[SimplyService] Starting analysis: ", analysisError)
		return 0, errors.New(analysisError.Message)
	}
	return requestCode, nil
}

//RetrieveExtraction attempts to retrieve an OCR Analysis result from Simply Atomics
func (ref *simplyService) RetrieveExtraction(requestCode int) (*contracts.SimplyResult, error) {
	result, analysisErr := ref.simplyClient.RetrieveExtraction(strconv.Itoa(requestCode))
	if analysisErr != nil {
		return nil, errors.New(analysisErr.Message)
	}
	return result, nil
}
