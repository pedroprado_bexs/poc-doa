package httpclient

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"

	_ "github.com/joho/godotenv/autoload"
	contracts "temis.doa/Domain/External/simply/contracts"
	"temis.doa/Infra/simply"
)

var (
	host        = os.Getenv("SIMPLY_HOST")
	loginURI    = os.Getenv("SIMPLY_LOGIN_URI")
	analysisURI = os.Getenv("SIMPLY_ANALYSIS_URI")
)

type ISimplyClient interface {
	BeginExtraction(buffer *bytes.Buffer, bufferWriter *multipart.Writer) (int, *contracts.SimplyError)
	RetrieveExtraction(requestCode string) (*contracts.SimplyResult, *contracts.SimplyError)
}

type simplyClient struct {
	webClient *http.Client
}

func init() {
}

func NewSimplyClient(webClient *http.Client) ISimplyClient {
	return &simplyClient{
		webClient: webClient,
	}
}

//BeginExtraction requests the start of an exctration by Simply Atomics
func (ref *simplyClient) BeginExtraction(buffer *bytes.Buffer, bufferWriter *multipart.Writer) (int, *contracts.SimplyError) {
	URL := host + analysisURI

	request, _ := http.NewRequest("POST", URL, buffer)
	request.Header.Set("Authorization", simply.GetAuthorization())
	request.Header.Set("Content-Type", bufferWriter.FormDataContentType())

	response, err := ref.webClient.Do(request)
	if err != nil {
		log.Println("[SimplyClient] Error sending analysis request: ", err)
	}

	if response.StatusCode != 200 {
		analysisError := &contracts.SimplyError{}
		err := json.NewDecoder(response.Body).Decode(&analysisError)
		if err != nil {
			log.Println("[SimplyClient] Error decoding response body: ", err)
		}
		return 0, analysisError
	}

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	bodyString := string(bodyBytes)
	requestCode, err := strconv.Atoi(bodyString)
	if err != nil {
		log.Println("[SimplyClient] Error getting analysis code: ", err)
	}

	return requestCode, nil
}

func (ref *simplyClient) RetrieveExtraction(requestCode string) (*contracts.SimplyResult, *contracts.SimplyError) {
	URL := host + analysisURI + "/" + requestCode

	request, _ := http.NewRequest("GET", URL, nil)
	request.Header.Set("Authorization", simply.GetAuthorization())

	response, err := ref.webClient.Do(request)
	if err != nil {
		log.Println("[SimplyClient] Error sending request: ", err)
	}

	if response.StatusCode != 200 {
		analysisError := &contracts.SimplyError{}
		err := json.NewDecoder(response.Body).Decode(&analysisError)
		if err != nil {
			log.Println("[SimplyClient] Error decoding response body: ", err)
		}
		return nil, analysisError
	}

	analysisResult := &contracts.SimplyResult{}
	err2 := json.NewDecoder(response.Body).Decode(&analysisResult)
	if err2 != nil {
		log.Println("[SimplyClient] Error decoding response body: ", err2)
	}
	return analysisResult, nil
}
