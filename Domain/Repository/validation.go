package repository

import (
	entity "temis.doa/Domain/Entity"
)

type IValidationRepository interface {
	Save(validation *entity.Validation) error
}

type validationRepository struct{}

func NewValidationRepository() IValidationRepository {
	return &validationRepository{}
}

func (ref *validationRepository) Save(validation *entity.Validation) error {
	return nil
}
