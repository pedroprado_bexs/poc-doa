package entity

//DocumentMetadata contains the CNH metadata
type Metadata struct {
	NomeTexto           string
	NomeMaeTexto        string
	DataNascimentoTexto string
	RegistroTexto       string
	IdentidadeTexto     string
}
