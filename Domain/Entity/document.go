package entity

import (
	"github.com/google/uuid"
)

//Document represents the File Entity to be analyzed
type Document struct {
	ProfileID   uuid.UUID
	Type        DocumentType
	Side        SideType
	Metadata    *Metadata
	File        *File
	OCRAnalysis *OCRAnalysis
}

//DocumentType represents the Document Types
type DocumentType string

const (
	RG  DocumentType = "RG"
	CNH DocumentType = "CNH"
)

//SideType represents the Document Sydes
type SideType string

const (
	FRENTE SideType = "FRENTE"
	VERSO  SideType = "VERSO"
	TUDO   SideType = "TUDO"
)
