package entity

import (
	"github.com/google/uuid"
)

type Validation struct {
	ID       uuid.UUID
	Document *Document
	Score    *Score
}
