package entity

import "github.com/google/uuid"

type File struct {
	FileID    uuid.UUID
	FileURL   string
	FileName  string
	FileBytes []byte
}
