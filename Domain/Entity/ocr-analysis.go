package entity

//OCRAnalysis represents the result of an OCR Analysis
type OCRAnalysis struct {
	RequestCode  int
	DocumentType DocumentType
	DocumentSide SideType
	RequestDate  string
	ProcessDate  string
	OcrMetadata  *OcrMetadata
}

//OcrMetadata is the metadata for an Ocr Analysis
type OcrMetadata struct {
	Assertividade       string
	NomeTexto           string
	NomeMaeTexto        string
	DataNascimentoTexto string
	RegistroTexto       string
	ExpedicaoTexto      string
	IdentidadeTexto     string
}
