package domaininfra

import (
	entity "temis.doa/Domain/Entity"
)

var channel chan *entity.Document

func init() {
	channel = make(chan *entity.Document)
}

func GetExtractorValidatorChannel() *chan *entity.Document {
	return &channel
}
